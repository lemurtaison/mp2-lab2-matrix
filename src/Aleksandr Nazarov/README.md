# Методы программирования 2: Верхнетреугольные матрицы на шаблонах

## Цели и задачи

В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное хранение матриц специального вида (верхнетреугольных) и выполнение основных операций над ними:

 - сложение/вычитание.
 - копирование.
 - сравнение.
 
В процессе выполнения лабораторной работы требуется использовать систему контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.

Перед выполнением работы студенты получают данный проект-шаблон, содержащий следующее:

 - Интерфейсы классов Вектор и Матрица (h-файл)
 - Начальный набор готовых тестов для каждого из указанных классов.
 - Набор заготовок тестов для каждого из указанных классов.
 - Тестовый пример использования класса Матрица.
 
Выполнение работы предполагает решение следующих задач:

 - Реализация методов шаблонного класса TVector согласно заданному интерфейсу.
 - Реализация методов шаблонного класса TMatrix согласно заданному интерфейсу.
 - Обеспечение работоспособности тестов и примера использования.
 - Реализация заготовок тестов, покрывающих все методы классов TVector и TMatrix.
 - Модификация примера использования в тестовое приложение, позволяющее задавать матрицы и осуществлять основные операции над ними.

## Реализация шаблонного класса TVector

```C++
#ifndef __TMATRIX_H__
#define __TMATRIX_H__

#include <iostream>

using namespace std;

const int MAX_VECTOR_SIZE = 100000000;
const int MAX_MATRIX_SIZE = 10000; 

// Шаблон вектора
template <class ValType>
class TVector
{
protected:
  ValType *pVector;
  int Size;       // размер вектора
  int StartIndex; // индекс первого элемента вектора
public:
  TVector(int s = 10, int si = 0);
  TVector(const TVector &v);                // конструктор копирования
  ~TVector();
  int GetSize()      { return Size;       } // размер вектора
  int GetStartIndex(){ return StartIndex; } // индекс первого элемента
  ValType& operator[](int pos);             // доступ
  bool operator==(const TVector &v) const;  // сравнение
  bool operator!=(const TVector &v) const;  // сравнение
  TVector& operator=(const TVector &v);     // присваивание

  // скалярные операции
  TVector  operator+(const ValType &val);   // прибавить скаляр
  TVector  operator-(const ValType &val);   // вычесть скаляр
  TVector  operator*(const ValType &val);   // умножить на скаляр

  // векторные операции
  TVector  operator+(const TVector &v);     // сложение
  TVector  operator-(const TVector &v);     // вычитание
  ValType  operator*(const TVector &v);     // скалярное произведение

  // ввод-вывод
  friend istream& operator>>(istream &in, TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      in >> v.pVector[i];
    return in;
  }
  friend ostream& operator<<(ostream &out, const TVector &v)
  {
    for (int i = 0; i < v.Size; i++)
      out << v.pVector[i] << ' ';
    return out;
  }
};

template <class ValType>
TVector<ValType>::TVector(int s, int si)
{
	if (s <= 0 || s > MAX_VECTOR_SIZE || si < 0 || si > MAX_VECTOR_SIZE)
		throw "wrong size";
	Size = s;
	StartIndex = si;
	pVector = NULL;
	pVector = new ValType[Size];
	if (pVector == NULL)
		throw "not enough memory";
	memset(pVector, 0, Size * sizeof(ValType));

} /*-------------------------------------------------------------------------*/

template <class ValType> //конструктор копирования
TVector<ValType>::TVector(const TVector<ValType> &v)
{
	Size = v.Size;
	StartIndex = v.StartIndex;
	pVector = NULL;
	pVector = new ValType[Size];
	if (pVector == NULL)
		throw "not enough memory";
	for (int i = 0; i < Size; i++)
		pVector[i] = v.pVector[i];
} /*-------------------------------------------------------------------------*/

template <class ValType>
TVector<ValType>::~TVector()
{
	delete[] pVector;
	pVector = NULL;
} /*-------------------------------------------------------------------------*/

template <class ValType> // доступ
ValType& TVector<ValType>::operator[](int pos)
{
	if (((pos - StartIndex) < 0) || ((pos - StartIndex) > Size))
		throw "wrong position";
	return pVector[pos - StartIndex];
} /*-------------------------------------------------------------------------*/

template <class ValType> // сравнение
bool TVector<ValType>::operator==(const TVector &v) const
{
	if ((Size != v.Size) || (StartIndex != v.StartIndex))
		return false;
	else
	{
		bool flag = true;
		for (int i = 0; ((i < Size) && (flag == true)); i++)
			if (pVector[i] != v.pVector[i])
				flag = false;
		return flag;
	}
} /*-------------------------------------------------------------------------*/

template <class ValType> // сравнение
bool TVector<ValType>::operator!=(const TVector &v) const
{
	return !operator==(v);
} /*-------------------------------------------------------------------------*/

template <class ValType> // присваивание
TVector<ValType>& TVector<ValType>::operator=(const TVector &v)
{
	if (this != &v)
	{
		if ((Size != v.Size) || (StartIndex != v.StartIndex))
		{
			Size = v.Size;
			StartIndex = v.StartIndex;
			delete[] pVector;
			pVector = NULL;
			pVector = new ValType[Size];
			if (pVector == NULL)
				throw "not enough memory";
		}
		for (int i = 0; i < Size; i++)
			pVector[i] = v.pVector[i];
	}
	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // прибавить скаляр
TVector<ValType> TVector<ValType>::operator+(const ValType &val)
{
	TVector<ValType> result(*this);
	for (int i = 0; i < Size; i++)
		result.pVector[i] += val;
	return result;
} /*-------------------------------------------------------------------------*/

template <class ValType> // вычесть скаляр
TVector<ValType> TVector<ValType>::operator-(const ValType &val)
{
	TVector<ValType> result(*this);
	for (int i = 0; i < Size; i++)
		result.pVector[i] -= val;
	return result;
} /*-------------------------------------------------------------------------*/

template <class ValType> // умножить на скаляр
TVector<ValType> TVector<ValType>::operator*(const ValType &val)
{
	TVector<ValType> result(*this);
	for (int i = 0; i < Size; i++)
		result.pVector[i] *= val;
	return result;
} /*-------------------------------------------------------------------------*/

template <class ValType> // сложение
TVector<ValType> TVector<ValType>::operator+(const TVector<ValType> &v)
{
	if ((v.Size != Size) || (StartIndex != v.StartIndex))
		throw "not equal vector";
	TVector<ValType> result(*this);
	for (int i = 0; i < Size; i++)
		result.pVector[i] = pVector[i] + v.pVector[i];
	return result;
} /*-------------------------------------------------------------------------*/

template <class ValType> // вычитание
TVector<ValType> TVector<ValType>::operator-(const TVector<ValType> &v)
{
	if ((v.Size != Size) || (StartIndex != v.StartIndex))
		throw "not equal vector";
	TVector<ValType> result(*this);
	for (int i = 0; i < Size; i++)
		result.pVector[i] = pVector[i] - v.pVector[i];
	return result;
} /*-------------------------------------------------------------------------*/

template <class ValType> // скалярное произведение
ValType TVector<ValType>::operator*(const TVector<ValType> &v)
{
	if ((v.Size != Size) || (StartIndex != v.StartIndex))
		throw "not equal vector";
	ValType add = 0;
	for (int i = 0; i < Size; i++)
		add += pVector[i] * v.pVector[i];
	return add;
} /*-------------------------------------------------------------------------*/
```
## Реализация шаблонного класса TMatrix
```C++
template <class ValType>
class TMatrix : public TVector<TVector<ValType> >
{
public:
  TMatrix(int s = 10);          
  TMatrix(const TMatrix &mt);                    // копирование
  TMatrix(const TVector<TVector<ValType> > &mt); // преобразование типа
  bool operator==(const TMatrix &mt) const;      // сравнение
  bool operator!=(const TMatrix &mt) const;      // сравнение
  TMatrix& operator= (const TMatrix &mt);        // присваивание
  TMatrix  operator+ (const TMatrix &mt);        // сложение
  TMatrix  operator- (const TMatrix &mt);        // вычитание


  // ввод / вывод
  friend istream& operator>>(istream &in, TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      in >> mt.pVector[i];
    return in;
  }
  friend ostream & operator<<( ostream &out, const TMatrix &mt)
  {
    for (int i = 0; i < mt.Size; i++)
      out << mt.pVector[i] << endl;
    return out;
  }
};

template <class ValType>
TMatrix<ValType>::TMatrix(int s): TVector<TVector<ValType> >(s)
{
	if (s <= 0 || s >= MAX_MATRIX_SIZE)
		throw "wrong size";
	Size = s;
	StartIndex = 0;
	for (int i = 0; i < s; i++)
		pVector[i] = TVector<ValType>(s - i,i);
} /*-------------------------------------------------------------------------*/

template <class ValType> // конструктор копирования
TMatrix<ValType>::TMatrix(const TMatrix<ValType> &mt):
  TVector<TVector<ValType> >(mt) 
{
}

template <class ValType> // конструктор преобразования типа
TMatrix<ValType>::TMatrix(const TVector<TVector<ValType> > &mt):
  TVector<TVector<ValType> >(mt) 
{
}

template <class ValType> // сравнение
bool TMatrix<ValType>::operator==(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType> >::operator==(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // сравнение
bool TMatrix<ValType>::operator!=(const TMatrix<ValType> &mt) const
{
	return TVector<TVector<ValType> >::operator!=(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // присваивание
TMatrix<ValType>& TMatrix<ValType>::operator=(const TMatrix<ValType> &mt)
{
	Size = mt.Size;
	for (int i = 0; i < Size; i++)
		pVector[i] = mt.pVector[i];
	return *this;
} /*-------------------------------------------------------------------------*/

template <class ValType> // сложение
TMatrix<ValType> TMatrix<ValType>::operator+(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType> >::operator+(mt);
} /*-------------------------------------------------------------------------*/

template <class ValType> // вычитание
TMatrix<ValType> TMatrix<ValType>::operator-(const TMatrix<ValType> &mt)
{
	return TVector<TVector<ValType> >::operator-(mt);
} /*-------------------------------------------------------------------------*/

// TVector О3 Л2 П4 С6
// TMatrix О2 Л2 П3 С3
#endif
```

##Реализация и обеспечение работоспособности тестов

### Реализация тестов

#### Файл test_main.cpp

```C++
#include "gtest.h"

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  RUN_ALL_TESTS();
  system("pause");
  return 0;
}
```

#### Файл test_tvector.cpp

```C++
#include "utmatrix.h"

#include "gtest.h"

TEST(TVector, can_create_vector_with_positive_length)
{
  ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
{
  ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
{
  ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
{
  ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
{
  TVector<int> v(10);

  ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
{
	TVector<int> a(5);
	a[1] = 1;
	TVector<int> b(a);
	EXPECT_EQ(a, b);
}

TEST(TVector, copied_vector_has_its_own_memory)
{
	TVector<int> a(5);
	TVector<int> b(a);
	b[1] = 2;
	a[1] = 4;
	EXPECT_NE(b[1], a[1]);
}

TEST(TVector, can_get_size)
{
  TVector<int> v(4);

  EXPECT_EQ(4, v.GetSize());
}

TEST(TVector, can_get_start_index)
{
  TVector<int> v(4, 2);

  EXPECT_EQ(2, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
{
  TVector<int> v(4);
  v[0] = 4;

  EXPECT_EQ(4, v[0]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
{
	TVector<int> v(5);
	ASSERT_ANY_THROW(v[-1] = 0);
}

TEST(TVector, throws_when_set_element_with_too_large_index)
{
	TVector<int> v(5);
	ASSERT_ANY_THROW(v[8] = 0);
}

TEST(TVector, can_assign_vector_to_itself)
{
	TVector<int> v(3);
	for (int i = 0; i < 3; i++)
		v[i] = 14;
	v = v;
	EXPECT_EQ(14, v[0]);
	EXPECT_EQ(14, v[1]);
	EXPECT_EQ(14, v[2]);
}

TEST(TVector, can_assign_vectors_of_equal_size)
{
	TVector<int> a(3);
	TVector<int> b(3);
	for (int i = 0; i < 3; i++)
		a[i] = 88;
	b = a;
	EXPECT_EQ(88, b[0]);
	EXPECT_EQ(88, b[1]);
	EXPECT_EQ(88, b[2]);
}

TEST(TVector, assign_operator_change_vector_size)
{
	TVector<int> a(5);
	TVector<int> b(10);
	a = b;
	EXPECT_EQ(10, a.GetSize());
}

TEST(TVector, can_assign_vectors_of_different_size)
{
	TVector<int> a(3);
	TVector<int> b(5);
	for (int i = 0; i < 3; i++)
		a[i] = 22;
	b = a;
	EXPECT_EQ(22, b[0]);
	EXPECT_EQ(22, b[1]);
	EXPECT_EQ(22, b[2]);
	EXPECT_EQ(3, b.GetSize());
}

TEST(TVector, compare_equal_vectors_return_true)
{
	TVector<int> a(5);
	TVector<int> b(5);
	EXPECT_EQ(a, b);
}

TEST(TVector, compare_vector_with_itself_return_true)
{
	TVector<int> a(5);
	EXPECT_EQ(a, a);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
{
	TVector<int> a(5);
	TVector<int> b(10);
	EXPECT_NE(a, b);
}

TEST(TVector, can_add_scalar_to_vector)
{
	TVector<int> a(1);
	TVector<int> b;
	a[0] = 1;
	b = a + 8;
	EXPECT_EQ(9, b[0]);
}

TEST(TVector, can_subtract_scalar_from_vector)
{
	TVector<int> a(1);
	TVector<int> b;
	a[0] = 10;
	b = a - 8;
	EXPECT_EQ(2, b[0]);
}

TEST(TVector, can_multiply_scalar_by_vector)
{
	TVector<int> a(1);
	TVector<int> b;
	a[0] = 10;
	b = a * 8;
	EXPECT_EQ(80, b[0]);
}

TEST(TVector, can_add_vectors_with_equal_size)
{
	TVector<int> a(2);
	TVector<int> b(2);
	TVector<int> c;
	a[0] = 2;
	b[1] = 2;
	c = a + b;
	EXPECT_EQ(2, c[0]);
	EXPECT_EQ(2, c[1]);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
{
	TVector<int> a(5);
	TVector<int> b(10);
	ASSERT_ANY_THROW(a + b);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
{
	TVector<int> a(2);
	TVector<int> b(2);
	TVector<int> c;
	a[0] = 2;
	b[0] = 2;
	a[1] = 6;
	b[1] = 2;
	c = a - b;
	EXPECT_EQ(0, c[0]);
	EXPECT_EQ(4, c[1]);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
{
	TVector<int> a(5);
	TVector<int> b(10);
	ASSERT_ANY_THROW(a - b);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
{
	TVector<int> a(2);
	TVector<int> b(2);
	int c;
	a[0] = 2;
	b[0] = 2;
	a[1] = 6;
	b[1] = 2;
	c = a * b;
	EXPECT_EQ(16, c);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
{
	TVector<int> a(5);
	TVector<int> b(10);
	ASSERT_ANY_THROW(a * b);
}


```

#### Файл test_tmatrix.cpp

```C++
#include "utmatrix.h"

#include "gtest.h"

TEST(TMatrix, can_create_matrix_with_positive_length)
{
  ASSERT_NO_THROW(TMatrix<int> m(5));
}

TEST(TMatrix, cant_create_too_large_matrix)
{
  ASSERT_ANY_THROW(TMatrix<int> m(MAX_MATRIX_SIZE + 1));
}

TEST(TMatrix, throws_when_create_matrix_with_negative_length)
{
  ASSERT_ANY_THROW(TMatrix<int> m(-5));
}

TEST(TMatrix, can_create_copied_matrix)
{
  TMatrix<int> m(5);

  ASSERT_NO_THROW(TMatrix<int> m1(m));
}

TEST(TMatrix, copied_matrix_is_equal_to_source_one)
{
	TMatrix<int> a(5);
	a[0][0] = 1;
	TMatrix<int> b(a);
	EXPECT_EQ(a, b);
}

TEST(TMatrix, copied_matrix_has_its_own_memory)
{
	TMatrix<int> a(5);
	TMatrix<int> b(a);
	b[0][0] = 2;
	a[0][0] = 4;
	EXPECT_NE(b[0][0], a[0][0]);
}

TEST(TMatrix, can_get_size)
{
	TMatrix<int> a(5);
	EXPECT_EQ(5, a.GetSize());
}

TEST(TMatrix, can_set_and_get_element)
{
	TMatrix<int> a(5);
	a[0][1] = 22;
	EXPECT_EQ(22, a[0][1]);
}

TEST(TMatrix, throws_when_set_element_with_negative_index)
{
	TMatrix<int> a(5);
	ASSERT_ANY_THROW(a[-1][0]);
}

TEST(TMatrix, throws_when_set_element_with_too_large_index)
{
	TMatrix<int> a(10);
	ASSERT_ANY_THROW(a[11][11]);
}

TEST(TMatrix, can_assign_matrix_to_itself)
{
	TMatrix<int> a(3);
	for (int i = 0; i < 3; i++)
		a[i][i] = 14;
	a = a;
	EXPECT_EQ(14, a[0][0]);
	EXPECT_EQ(14, a[1][1]);
	EXPECT_EQ(14, a[2][2]);
}

TEST(TMatrix, can_assign_matrices_of_equal_size)
{
	TMatrix<int> a(3);
	TMatrix<int> b(3);
	for (int i = 0; i < 3; i++)
		a[i][i] = 14;
	b = a;
	EXPECT_EQ(14, b[0][0]);
	EXPECT_EQ(14, b[1][1]);
	EXPECT_EQ(14, b[2][2]);
}

TEST(TMatrix, assign_operator_change_matrix_size)
{
	TMatrix<int> a(3);
	TMatrix<int> b(6);
	for (int i = 0; i < 3; i++)
		a[i][i] = 14;
	b = a;
	EXPECT_EQ(3, b.GetSize());
}

TEST(TMatrix, can_assign_matrices_of_different_size)
{
	TMatrix<int> a(3);
	TMatrix<int> b(6);
	for (int i = 0; i < 3; i++)
		a[i][i] = 14;
	b = a;
	EXPECT_EQ(14, b[0][0]);
	EXPECT_EQ(14, b[1][1]);
	EXPECT_EQ(14, b[2][2]);
	EXPECT_EQ(3, b.GetSize());
}

TEST(TMatrix, compare_equal_matrices_return_true)
{
	TMatrix<int> a(5);
	TMatrix<int> b(5);
	EXPECT_EQ(a, b);
}

TEST(TMatrix, compare_matrix_with_itself_return_true)
{
	TMatrix<int> a(5);
	EXPECT_EQ(a, a);
}

TEST(TMatrix, matrices_with_different_size_are_not_equal)
{
	TMatrix<int> a(5);
	TMatrix<int> b(8);
	EXPECT_NE(a, b);
}

TEST(TMatrix, can_add_matrices_with_equal_size)
{
	TMatrix<int> a(2);
	TMatrix<int> b(2);
	TMatrix<int> c;
	a[0][0] = 2;
	b[1][1] = 2;
	c = a + b;
	EXPECT_EQ(2, c[0][0]);
	EXPECT_EQ(2, c[1][1]);
	EXPECT_EQ(0, c[0][1]);
}

TEST(TMatrix, cant_add_matrices_with_not_equal_size)
{
	TMatrix<int> a(2);
	TMatrix<int> b(4);
	ASSERT_ANY_THROW(a + b);
}

TEST(TMatrix, can_subtract_matrices_with_equal_size)
{
	TMatrix<int> a(2);
	TMatrix<int> b(2);
	TMatrix<int> c;
	a[0][0] = 4;
	b[0][0] = 2;
	a[1][1] = 6;
	b[1][1] = 2;
	c = a - b;
	EXPECT_EQ(2, c[0][0]);
	EXPECT_EQ(4, c[1][1]);
	EXPECT_EQ(0, c[0][1]);
}

TEST(TMatrix, cant_subtract_matrixes_with_not_equal_size)
{
	TMatrix<int> a(2);
	TMatrix<int> b(4);
	ASSERT_ANY_THROW(a - b);
}

```

### Результаты тестов

![tests](http://i.imgur.com/jxI8oPH.png)

##Обеспечение работоспособности примера использования

###Результат примера использования

![test_work](http://i.imgur.com/BztS4eG.png)

## Модификация примера использования в тестовое приложение, позволяющее задавать матрицы и осуществлять основные операции над ними

###Реализация тестового приложения

####Файл simple_matrix.cpp

```C++
#include <string>
#include <iostream>
#include "utmatrix.h"
#include "gtest.h"
#include <map>
#include "test_program_function.h"

using namespace std;

int main()
{
	string console, command, arg[5] = { "", "", "", "", "" };
	map<string, TMatrix<int>> matrix;
	setlocale(LC_ALL, "Russian");
	cout << "Модифицированная программа тестирования поддержки верхнетреугольной матрицы." << endl;
	cout << "Для получения информации о возможностях введите help." << endl;
	do
	{
		command.clear();
		console.clear();
		getline(cin, console);
		if (console.size())
		{
			int i, f = 0;
			for (i = 0; i < console.size() && console.at(i) != (' '); i++)
				command.push_back(console.at(i));
			i++;
			for (; i < console.size() && f < 5; i++)
			{
				if (console.at(i) != ' ')
					arg[f].push_back(console.at(i));
				else f++;
			}
			try
			{
				if (command_list(command)) throw ("комманды " + command + " не существует");
				if (f == 5) throw "неверное количество аргументов";
				if (command == "help") help();
				if (command == "create") create(arg, matrix);
				if (command == "delete") del(arg, matrix);
				if (command == "info") info(arg, matrix);
				if (command == "add") add(arg, matrix);
				if (command == "subtract") subtract(arg, matrix);
				if (command == "push") push(arg, matrix);
				if (command == "equal") equal(arg, matrix);
				if (command == "assigned") assigned(arg, matrix);
			}
			catch (string err) { cout << err << endl; }
			catch (char *err) { cout << err << endl; }
		}
		for (int i = 0; i < 5; i++)
			arg[i] = "";
	} while (command != "exit");
	return 0;
}

```

####Файл test_program_function.h

```C++
#include <string>
#include <iostream>
#include "utmatrix.h"
#include "gtest.h"
#include <map>

using namespace std;

void help();
void create(string *arg, map<string, TMatrix<int>> &matrix);
void del(string *arg, map<string, TMatrix<int>> &matrix);
void info(string *arg, map<string, TMatrix<int>> matrix);
void add(string *arg, map<string, TMatrix<int>> &matrix);
void subtract(string *arg, map<string, TMatrix<int>> &matrix);
void push(string *arg, map<string, TMatrix<int>> &matrix);
void equal(string *arg, map<string, TMatrix<int>> matrix);
bool legal_name(string str);
int str_to_int(string str);
bool can_str_to_int(string str);
bool command_list(string command);
void assigned(string *arg, map<string, TMatrix<int>> &matrix);

void help()
{
	cout << "Комманды: " << endl;
	cout << "'create NAME X' - создать матрицу с именем NAME размера X" << endl;
	cout << "'delete NAME' - удалить матрицу с именем NAME" << endl;
	cout << "'assigned X1 X2' - копировать данные матрицы X2 в матрицу X1" << endl;
	cout << "'info' - список созданных матриц" << endl;
	cout << "'info X1' - информация о матрице" << endl;
	cout << "'add X1 X2 X3' - сложить матрицу/константу X1 с матрицей/константой X2 и поместить в матрицу X3" << endl;
	cout << "'add X1 X2' - сложить матрицу/константу X1 с матрицей/константой X2 и вывести на экран результат" << endl;
	cout << "'subtract X1 X2 X3' - вычесть матрицу/константу X1 с матрицей/константой X2 и поместить в матрицу X3" << endl;
	cout << "'subtract X1 X2' - вычесть матрицу/константу X1 с матрицей/константой X2 и вывести на экран результат" << endl;
	cout << "'push NAME x y z' - присвоить ячейке(x,y) матрицы с именем NAME значение z" << endl;
	cout << "'equal X1 X2' - сравнить матрицу X1 с матрицей X2 и вывести результат на экран" << endl;
	cout << "'exit' - выйти из программы" << endl;
}
void create(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[0] == "" || arg[1] == "" || arg[3] != "")
		throw "неверное количество аргументов";
	if (legal_name(arg[0]))
	{
		map<string, TMatrix<int>>::iterator it = matrix.find(arg[0]);
		if (it == matrix.end())
			matrix.insert(make_pair(arg[0], TMatrix<int>(str_to_int(arg[1]))));
		else
			throw "эта матрица уже существует";
	}
	else
		throw (arg[0] + " - неверное название матрицы");
}
void del(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[0] == "" || arg[1] != "")
		throw "неверное количество аргументов";
	if (matrix.empty())
		throw "матрицы " + arg[0] + " не существует";
	map<string, TMatrix<int>>::iterator it1;
	it1 = matrix.find(arg[0]);
	if (it1 != matrix.end())
	{
		map<string, TMatrix<int>> buffer;
		map<string, TMatrix<int>>::iterator it2 = matrix.begin();
		for (; it2 != matrix.end(); ++it2)
		{
			if (it1 != it2)
				buffer.insert(*it2);
		}
		matrix = buffer;
	}
	else
		throw ("матрицы " + arg[0] + " не существует");
}
void info(string *arg, map<string, TMatrix<int>> matrix)
{
	if (matrix.empty()) throw "данных нет";
	if (arg[1] != "") throw "неверное количество аргументов";
	if (arg[0] == "")
	{
		map<string, TMatrix<int>>::iterator it = matrix.begin();
		cout << "=======================" << endl;
		for (; it != matrix.end(); ++it)
			cout << it->first << endl;
		cout << "=======================" << endl;
	}
	else
	{
		map<string, TMatrix<int>>::iterator it;
		it = matrix.find(arg[0]);
		if (it != matrix.end())
		{
			cout << "=======================" << endl;
			cout << it->second << endl;
			cout << "=======================" << endl;
		}
		else
			throw ("матрицы " + arg[0] + " не существует");
	}
}
void add(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[1] == "" || arg[3] != "") throw "неверное количество аргументов";
	if (arg[2] == "")
	{
		if (can_str_to_int(arg[0]) && can_str_to_int(arg[1]))
			cout << (str_to_int(arg[0]) + str_to_int(arg[1])) << endl;
		if (!((legal_name(arg[0]) || legal_name(arg[1])) && matrix.size())) throw ((legal_name(arg[0])) ? ("матрицы " + arg[0] + " не существует") : ("матрицы " + arg[1] + " не существует"));
		if (can_str_to_int(arg[0]) && legal_name(arg[1]))
		{
			int num = str_to_int(arg[0]);
			map<string, TMatrix<int>>::iterator it = matrix.find(arg[1]);
			if (it != matrix.end())
			{
				cout << "=======================" << endl;
				for (int i = 0; i < it->second.GetSize(); i++)
					cout << (it->second[i] + num) << endl;
				cout << "=======================" << endl;
			}
			else throw ("матрицы " + arg[1] + " не существует");
		}
		if (can_str_to_int(arg[1]) && legal_name(arg[0]))
		{
			int num = str_to_int(arg[1]);
			map<string, TMatrix<int>>::iterator it = matrix.find(arg[0]);
			if (it != matrix.end())
			{
				cout << "=======================" << endl;
				for (int i = 0; i < it->second.GetSize(); i++)
					cout << (it->second[i] + num) << endl;
				cout << "=======================" << endl;
			}
			else throw ("матрицы " + arg[1] + " не существует");
		}
		if (legal_name(arg[0]) && legal_name(arg[1]))
		{
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
			if (it1 != matrix.end() && it2 != matrix.end())
			{
				cout << "=======================" << endl;
				cout << (it1->second + it2->second) << endl;
				cout << "=======================" << endl;
			}
			else throw ((it1 == matrix.end()) ? ("матрицы " + arg[0] + " не существует") : ("матрицы " + arg[1] + " не существует"));
		}
	}
	else
	{
		map<string, TMatrix<int>>::iterator it = matrix.find(arg[2]);
		if (can_str_to_int(arg[0]) && can_str_to_int(arg[1]))
			throw "сумму двух чисел нельзя конвертировать в матрицу";
		if (matrix.size())
		{
			if (it == matrix.end())
				throw ("матрицы " + arg[2] + " не существует");
		}
		else
		{
			throw ("матрицы " + arg[2] + " не существует");
		}
		if (can_str_to_int(arg[0]) && legal_name(arg[1]))
		{
			int num = str_to_int(arg[0]);
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[1]);
			if (it1 != matrix.end())
			{
				for (int i = 0; i < it1->second.GetSize(); i++)
					it->second[i] = (it1->second[i] + num);
			}
			else throw ("матрицы " + arg[1] + " не существует");
		}
		if (can_str_to_int(arg[1]) && legal_name(arg[0]))
		{
			int num = str_to_int(arg[1]);
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			if (it1 != matrix.end())
			{
				for (int i = 0; i < it1->second.GetSize(); i++)
					it->second[i] = (it1->second[i] + num);
			}
			else throw ("матрицы " + arg[0] + " не существует");
		}
		if (legal_name(arg[0]) && legal_name(arg[1]))
		{
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
			if (it1 != matrix.end() && it2 != matrix.end())
				it->second = (it1->second + it2->second);
			else throw ((it1 == matrix.end()) ? ("матрицы " + arg[0] + " не существует") : ("матрицы " + arg[1] + " не существует"));
		}
	}
}
void subtract(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[1] == "" || arg[3] != "") throw "неверное количество аргументов";
	if (arg[2] == "")
	{
		if (can_str_to_int(arg[0]) && can_str_to_int(arg[1]))
			cout << (str_to_int(arg[0]) + str_to_int(arg[1])) << endl;
		if (!((legal_name(arg[0]) || legal_name(arg[1])) && matrix.size())) throw ((legal_name(arg[0])) ? ("матрицы " + arg[0] + " не существует") : ("матрицы " + arg[1] + " не существует"));
		if (can_str_to_int(arg[0]) && legal_name(arg[1]))
		{
			int num = str_to_int(arg[0]);
			map<string, TMatrix<int>>::iterator it = matrix.find(arg[1]);
			if (it != matrix.end())
			{
				cout << "=======================" << endl;
				for (int i = 0; i < it->second.GetSize(); i++)
					cout << (it->second[i] - num) << endl;
				cout << "=======================" << endl;
			}
			else throw ("матрицы " + arg[1] + " не существует");
		}
		if (can_str_to_int(arg[1]) && legal_name(arg[0]))
		{
			int num = str_to_int(arg[1]);
			map<string, TMatrix<int>>::iterator it = matrix.find(arg[0]);
			if (it != matrix.end())
			{
				cout << "=======================" << endl;
				for (int i = 0; i < it->second.GetSize(); i++)
					cout << (it->second[i] - num) << endl;
				cout << "=======================" << endl;
			}
			else throw ("матрицы " + arg[1] + " не существует");
		}
		if (legal_name(arg[0]) && legal_name(arg[1]))
		{
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
			if (it1 != matrix.end() && it2 != matrix.end())
			{
				cout << "=======================" << endl;
				cout << (it1->second - it2->second) << endl;
				cout << "=======================" << endl;
			}
			else throw ((it1 == matrix.end()) ? ("матрицы " + arg[0] + " не существует") : ("матрицы " + arg[1] + " не существует"));
		}
	}
	else
	{
		map<string, TMatrix<int>>::iterator it = matrix.find(arg[2]);
		if (can_str_to_int(arg[0]) && can_str_to_int(arg[1]))
			throw "сумму двух чисел нельзя конвертировать в матрицу";
		if (matrix.size())
		{
			if (it == matrix.end())
				throw ("матрицы " + arg[2] + " не существует");
		}
		else
		{
			throw ("матрицы " + arg[2] + " не существует");
		}
		if (can_str_to_int(arg[0]) && legal_name(arg[1]))
		{
			int num = str_to_int(arg[0]);
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[1]);
			if (it1 != matrix.end())
			{
				for (int i = 0; i < it1->second.GetSize(); i++)
					it->second[i] = (it1->second[i] - num);
			}
			else throw ("матрицы " + arg[1] + " не существует");
		}
		if (can_str_to_int(arg[1]) && legal_name(arg[0]))
		{
			int num = str_to_int(arg[1]);
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			if (it1 != matrix.end())
			{
				for (int i = 0; i < it1->second.GetSize(); i++)
					it->second[i] = (it1->second[i] - num);
			}
			else throw ("матрицы " + arg[0] + " не существует");
		}
		if (legal_name(arg[0]) && legal_name(arg[1]))
		{
			map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
			map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
			if (it1 != matrix.end() && it2 != matrix.end())
				it->second = (it1->second - it2->second);
			else throw ((it1 == matrix.end()) ? ("матрицы " + arg[0] + " не существует") : ("матрицы " + arg[1] + " не существует"));
		}
	}
}
void push(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[3] == "" || arg[4] != "")
		throw "неверное количество аргументов";
	if (legal_name(arg[0]) && can_str_to_int(arg[1]) && can_str_to_int(arg[2]) && can_str_to_int(arg[3]))
	{
		map<string, TMatrix<int>>::iterator it = matrix.find(arg[0]);
		if (it != matrix.end())
			it->second[str_to_int(arg[1])][str_to_int(arg[2])] = str_to_int(arg[3]);
		else
			throw ("матрица " + arg[0] + "не существует");
	}
	else throw "неправильные аргументы";
}
void equal(string *arg, map<string, TMatrix<int>> matrix)
{
	if (arg[1] == "" || arg[2] != "")
		throw "неверное количество аргументов";
	if (legal_name(arg[0]) && legal_name(arg[1]))
	{
		map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
		map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
		if (it1 != matrix.end() && it2 != matrix.end())
		{
			cout << (it1->second == it2->second) << endl;
		}
		else
			throw (it1 == matrix.end()) ? ("матрица " + arg[0] + "не существует") : ("матрица " + arg[1] + "не существует");
	}
	else throw "неправильные аргументы";
}
void assigned(string *arg, map<string, TMatrix<int>> &matrix)
{
	if (arg[1] == "" || arg[2] != "")
		throw "неверное количество аргументов";
	if (legal_name(arg[0]) && legal_name(arg[1]))
	{
		map<string, TMatrix<int>>::iterator it1 = matrix.find(arg[0]);
		map<string, TMatrix<int>>::iterator it2 = matrix.find(arg[1]);
		if (it1 != matrix.end() && it2 != matrix.end())
			it1->second = it2->second;
		else
			throw (it1 == matrix.end()) ? ("матрица " + arg[0] + "не существует") : ("матрица " + arg[1] + "не существует");
	}
	else throw "неправильные аргументы";
}
bool legal_name(string str)
{
	if ((str.at(0) >= 'A' && str.at(0) <= 'Z') || (str.at(0) >= 'a' && str.at(0) <= 'z'))
	{
		for (int i = 1; i < str.size(); i++)
		{
			if (!((str.at(i) >= 'A' && str.at(i) <= 'Z') || (str.at(i) >= 'a' && str.at(i) <= 'z') || (str.at(i) >= '0' && str.at(i) <= '9') || str.at(i) == '_'))
			{
				return false;
			}
		}
		return true;
	}
	else
		return false;
}
int str_to_int(string str)
{
	int result = 0;
	if (str.at(0) != '-')
	{
		for (int i = str.size() - 1, f = 0; i >= 0; i--, f++)
		{
			if (str.at(i) >= '0' && str.at(i) <= '9')
				result = result + pow(10, f)*(str.at(i) - '0');
			else
				throw ("аргумент " + str + " должен быть числом");
		}
	}
	else
	{
		for (int i = str.size() - 1, f = 0; i > 0; i--, f++)
		{
			if (str.at(i) >= '0' && str.at(i) <= '9')
				result = result - pow(10, f)*(str.at(i) - '0');
			else
				throw ("аргумент " + str + " должен быть числом");
		}
	}
	return result;
}
bool can_str_to_int(string str)
{
		for (int i = ((str.at(0) != '-')?0:1); i < str.size(); i++)
			if (str.at(i) < '0' || str.at(i) > '9')
				return false;
	return true;
}
bool command_list(string command)
{
	string commands[10] = { "create", "delete", "info", "add", "subtract", "push", "equal", "exit", "help", "assigned" };
	for (int i = 0; i < 10; i++)
		if (command == commands[i])
			return false;
	return true;
}

```

###Результат работы тестового приложения

![test_work](http://i.imgur.com/qa2kHoD.png)

##Вывод

Успешно реализована и протестирована матрица верхнетреугольного вида и основные операции над ней. Отработаны навыки работы с шаблонами и с google tests. Получен небольшой опыт работы с контейнерами map во время работы над тестовым приложением.